include .env
export $(shell sed 's/=.*//' .env)

DOCKER := $(shell command -v docker 2> /dev/null)
DOCKER_COMPOSE := $(shell command -v docker-compose 2> /dev/null) -p ${PROJECT_NAME}

help: ## Показать справку
	@awk 'BEGIN {FS = ":.*##"; printf "\nUsage:\n  make \033[36m\033[0m\n"} /^[$$()% 0-9a-zA-Z_-]+:.*?##/ { printf "  \033[36m%-15s\033[0m %s\n", $$1, $$2 } /^##@/ { printf "\n\033[1m%s\033[0m\n", substr($$0, 5) } ' $(MAKEFILE_LIST)

build: ## Запустить сборку всех контейнеров
	$(DOCKER_COMPOSE) build

up: ## Запустить все контейнеры
	$(DOCKER_COMPOSE) up --no-recreate -d

down: ## Выключить все контейнеры
	$(DOCKER_COMPOSE) down

restart: down ## Перезапустить все контейнеры
	@make up

sh: ## Запустить Shell, в качестве параметра нужно указать название нужного контейнера (пример: "make sh php-fpm")
	$(DOCKER_COMPOSE) exec "$(filter-out $@,$(MAKECMDGOALS))" /bin/sh

bash: ## Запустить Bash, в качестве параметра нужно указать название нужного контейнера (пример: "make bash php-fpm")
	$(DOCKER_COMPOSE) exec "$(filter-out $@,$(MAKECMDGOALS))" bash

restore-mysql-dump: ## Залить дамп, в качестве параметра нужно указать путь к файлу дампа на хосте
	$(DOCKER) exec -i $(shell command $(DOCKER_COMPOSE) ps -q mysql) sh -c 'exec mysql -uroot -p$(MYSQL_ROOT_PASSWORD) $(MYSQL_DATABASE)' < $(filter-out $@,$(MAKECMDGOALS))

composer: ## Composer
	$(DOCKER_COMPOSE) exec php-fpm composer $(filter-out $@,$(MAKECMDGOALS))

composer-install: ## composer install
	$(DOCKER_COMPOSE) exec php-fpm composer install

composer-update: ## composer update
	$(DOCKER_COMPOSE) exec php-fpm composer update

composer-dump-autoload: ## composer dump-autoload
	$(DOCKER_COMPOSE) exec php-fpm composer dump-autoload

artisan: ## Artisan
	$(DOCKER_COMPOSE) exec php-fpm php artisan $(filter-out $@,$(MAKECMDGOALS))

migrate: ## Выполнить миграции
	$(DOCKER_COMPOSE) exec php-fpm php artisan migrate

migrate-rollback: ## Откатить миграции
	$(DOCKER_COMPOSE) exec php-fpm php artisan migrate:rollback

langjs: ## Генерация js файла локализации на основе файлов локализации Laravel
	$(DOCKER_COMPOSE) exec php-fpm php artisan lang:js --no-lib
