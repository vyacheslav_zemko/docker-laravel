version: '3.5'

networks:
  frontend:
    driver: ${NETWORKS_DRIVER}
    external:
      name: frontend
  backend:
    driver: ${NETWORKS_DRIVER}

volumes:
  mysql:
    driver: ${VOLUMES_DRIVER}
  redis:
    driver: ${VOLUMES_DRIVER}
  phpmyadmin:
    driver: ${VOLUMES_DRIVER}

services:

### PHP-FPM ##############################################
    php-fpm:
      build:
        context: ./php-fpm
        args:
          - PROJECT_PHP_VERSION=${PHP_VERSION}
          - INSTALL_XDEBUG=${PHP_FPM_INSTALL_XDEBUG}
          - INSTALL_MYSQLI=${PHP_FPM_INSTALL_MYSQLI}
          - INSTALL_IMAGE_OPTIMIZERS=${PHP_FPM_INSTALL_IMAGE_OPTIMIZERS}
          - INSTALL_IMAGEMAGICK=${PHP_FPM_INSTALL_IMAGEMAGICK}
          - INSTALL_TIMECOP=${PHP_FPM_INSTALL_TIMECOP}
          - PUID=${PHP_FPM_PUID}
          - PGID=${PHP_FPM_PGID}
          - IMAGEMAGICK_VERSION=${PHP_FPM_IMAGEMAGICK_VERSION}
          - LOCALE=${PHP_FPM_DEFAULT_LOCALE}
          - http_proxy
          - https_proxy
          - no_proxy
      volumes:
        - ./php-fpm/php${PHP_VERSION}.ini:/usr/local/etc/php/php.ini
        - ${APP_CODE_PATH_HOST}:${APP_CODE_PATH_CONTAINER}${APP_CODE_CONTAINER_FLAG}
      ports:
        - "${PHP_FPM_XDEBUG_PORT}:9003"
      expose:
        - "9000"
      extra_hosts:
        - "host.docker.internal:host-gateway"
      environment:
        - PHP_IDE_CONFIG=${PHP_IDE_CONFIG}
        - DOCKER_TLS_VERIFY=1
        - DOCKER_TLS_CERTDIR=/certs
        - DOCKER_CERT_PATH=/certs/client
      networks:
        - backend
        - frontend

### PHP Worker ############################################
    php-worker:
      build:
        context: ./php-worker
        args:
          - PROJECT_PHP_VERSION=${PHP_VERSION}
          - INSTALL_GD=${PHP_WORKER_INSTALL_GD}
          - INSTALL_IMAGEMAGICK=${PHP_WORKER_INSTALL_IMAGEMAGICK}
          - PUID=${PHP_WORKER_PUID}
          - PGID=${PHP_WORKER_PGID}
          - IMAGEMAGICK_VERSION=${PHP_WORKER_IMAGEMAGICK_VERSION}
      volumes:
        - ${APP_CODE_PATH_HOST}:${APP_CODE_PATH_CONTAINER}${APP_CODE_CONTAINER_FLAG}
        - ./php-worker/supervisord.d:/etc/supervisord.d
      extra_hosts:
        - "host.docker.internal:host-gateway"
      depends_on:
        - redis
      networks:
        - backend

### NGINX Server #########################################
    nginx:
      build:
        context: ./nginx
        args:
          - PHP_UPSTREAM_CONTAINER=${NGINX_PHP_UPSTREAM_CONTAINER}
          - PHP_UPSTREAM_PORT=${NGINX_PHP_UPSTREAM_PORT}
          - http_proxy
          - https_proxy
          - no_proxy
      volumes:
        - ${APP_CODE_PATH_HOST}:${APP_CODE_PATH_CONTAINER}${APP_CODE_CONTAINER_FLAG}
        - ${NGINX_HOST_LOG_PATH}:/var/log/nginx
        - ${NGINX_SITES_PATH}:/etc/nginx/sites-available
        - ${NGINX_SSL_PATH}:/etc/nginx/ssl
      labels:
        - "traefik.enable=true"
        - "traefik.http.routers.${PROJECT_NAME}.entrypoints=https"
        - "traefik.http.routers.${PROJECT_NAME}.rule=Host(${HOSTS})"
        - "traefik.http.routers.${PROJECT_NAME}.tls=true"
        - "traefik.http.middlewares.${PROJECT_NAME}-compress.compress=true"
        - "traefik.http.routers.${PROJECT_NAME}.middlewares=${PROJECT_NAME}-compress"
        - "traefik.http.services.${PROJECT_NAME}.loadbalancer.server.port=80"
        - "traefik.docker.network=frontend"
        - "traefik.backend=nginx"
      depends_on:
        - php-fpm
      networks:
        - frontend
        - backend

### MySQL ################################################
    mysql:
      build:
        context: ./mysql
        args:
          - MYSQL_VERSION=${MYSQL_VERSION}
      command: --default-authentication-plugin=mysql_native_password
      environment:
        - MYSQL_DATABASE=${MYSQL_DATABASE}
        - MYSQL_USER=${MYSQL_USER}
        - MYSQL_PASSWORD=${MYSQL_PASSWORD}
        - MYSQL_ROOT_PASSWORD=${MYSQL_ROOT_PASSWORD}
        - TZ=UTC
      volumes:
        - ${DATA_PATH_HOST}/mysql:/var/lib/mysql
        - ${MYSQL_ENTRYPOINT_INITDB}:/docker-entrypoint-initdb.d
      ports:
        - "${MYSQL_PORT}:3306"
      networks:
        - backend

  ### Redis ################################################
    redis:
      build: ./redis
      volumes:
        - ${DATA_PATH_HOST}/redis:/data
      ports:
        - "${REDIS_PORT}:6379"
      networks:
        - backend

### phpMyAdmin ###########################################
    phpmyadmin:
      build: ./phpmyadmin
      environment:
        - PMA_ABSOLUTE_URI=https://${PMA_ADDRESS}
        - PMA_HOST=mysql
        - PMA_USER=${MYSQL_USER}
        - PMA_PASSWORD=${MYSQL_PASSWORD}
        - MAX_EXECUTION_TIME=${PMA_MAX_EXECUTION_TIME}
        - MEMORY_LIMIT=${PMA_MEMORY_LIMIT}
        - UPLOAD_LIMIT=${PMA_UPLOAD_LIMIT}
      depends_on:
        - mysql
      links:
        - mysql:mysql
      networks:
        - frontend
        - backend
      labels:
        - "traefik.enable=true"
        - "traefik.http.routers.pma-${PROJECT_NAME}.entrypoints=https"
        - "traefik.http.routers.pma-${PROJECT_NAME}.rule=Host(`${PMA_ADDRESS}`)"
        - "traefik.http.routers.pma-${PROJECT_NAME}.tls=true"
        - "traefik.http.middlewares.pma-${PROJECT_NAME}-compress.compress=true"
        - "traefik.http.routers.pma-${PROJECT_NAME}.middlewares=pma-${PROJECT_NAME}-compress"
        - "traefik.http.services.pma-${PROJECT_NAME}.loadbalancer.server.port=80"
        - "traefik.docker.network=frontend"
        - "traefik.backend=phpmyadmin"

### REDISWEBUI ################################################
    redis-webui:
      build:
        context: ./redis-webui
      environment:
        - REDIS_1_HOST=${REDIS_WEBUI_CONNECT_HOST}
        - REDIS_1_PORT=${REDIS_WEBUI_CONNECT_PORT}
      networks:
        - backend
        - frontend
      depends_on:
        - redis
      labels:
        - "traefik.enable=true"
        - "traefik.http.routers.redis-${PROJECT_NAME}.entrypoints=https"
        - "traefik.http.routers.redis-${PROJECT_NAME}.rule=Host(`${REDIS_WEBUI_ADDRESS}`)"
        - "traefik.http.routers.redis-${PROJECT_NAME}.tls=true"
        - "traefik.http.middlewares.redis-${PROJECT_NAME}-compress.compress=true"
        - "traefik.http.routers.redis-${PROJECT_NAME}.middlewares=redis-${PROJECT_NAME}-compress"
        - "traefik.http.services.redis-${PROJECT_NAME}.loadbalancer.server.port=80"
        - "traefik.docker.network=frontend"
        - "traefik.backend=redis-webui"
